#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
    printf("Hello!\n");
    int uid = getuid();
    int euid = geteuid();
    int gid = getgid();
    int egid = getegid();

    printf("uid: %d\n", uid);
    printf("euid: %d\n", euid);
    printf("gid: %d\n", gid);
    printf("egid: %d\n", egid);

    FILE *fp;
	char s;
	
	fp=fopen("/home/ubuntu/mydir/data.txt","r");
    if(fp==NULL) {
		printf("\nCAN NOT OPEN FILE");
	}
	do {
		s=getc(fp);
		printf("%c",s);
	}

	while(s!=EOF);
	fclose(fp);

    
    return 0;
}
