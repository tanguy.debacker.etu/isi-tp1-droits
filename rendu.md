# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Debacker Tanguy, email: tanguy.debacker.etu@univ-lille.fr

- Ezzinaoui Hamza, email: hamza.ezzinaoui.etu@univ-lille.fr
## Question 1

Le processus ne peut pas écrire car le seul droit autorisé pour l'utilisateur toto est la lecture (r). Les droits du groupe ubuntu sont lecture + écriture (rw), mais les droits attribués à l'utilisateur priment.

## Question 2
Quand on essaye d'accéder à *mydir* en tant que *toto*:
- `bash: cd: mydir/: Permission denied`

Le droit d'exécution donne l'accès au dossier, c'est donc un résultat attendu.

Quand on tape `ls -al mydir`:
- `ls: cannot access 'mydir/.': Permission denied`
- `ls: cannot access 'mydir/..': Permission denied`
- `ls: cannot access 'mydir/data.txt': Permission denied`
- `total 0`
- `d????????? ? ? ? ?            ? .`
- `d????????? ? ? ? ?            ? ..`
- `-????????? ? ? ? ?            ? data.txt`

On voit que l'accès à *mydir* est également restreint via `ls`.

## Question 3

Valeurs des différents ids sous *toto*: 
- uid: 1001
- euid: 1001
- gid: 1001
- egid: 1001

`Segmentation fault (core dumped)` quand on essaye de lire le fichier.


## Question 4

EUID =  1001
EGID =  1001
-passer par un fichier executable qui a le flag set-user-id .
## Question 5
-Modifier les informations personnel d'un utilisateur :
	Nom complet : 
	N° de bureau []: 
	Téléphone professionnel []: 
	Téléphone personnel []:
	
- commande : ls -al /usr/bin/chfn    
output:
-rwsr-xr-x 1 root root 85064 mai   28  2020 /usr/bin/chfn

tout processus exécutant ce fichier peut effectuer ses tâches avec les permissions associées à root "s" setuid.

les informations ont bien été mise a jour.


## Question 6

-les mots de passe cryptés ne sont stockés dans /etc/passwd qui doit etre accessible en lecture par tous,La commande /usr/sbin/pwconv est chargée de transférer les mots de passes cryptés, dans /etc/shadow

## Question 7

les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








